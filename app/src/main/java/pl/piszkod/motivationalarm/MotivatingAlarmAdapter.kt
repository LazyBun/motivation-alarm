package pl.piszkod.motivationalarm

import android.app.FragmentManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import uy.klutter.core.common.with

class MotivatingAlarmAdapter(ctx: Context, private val items: MutableList<MotivatingAlarm>, val fragmentManager: FragmentManager): ArrayAdapter<MotivatingAlarm>(ctx, 0, items) {

    val sharedPrefs: SharedPreferences by lazy {
        ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    private class ViewHolder(row: View?) {
        var txtName: TextView? = null
        var editButton: Button? = null
        var cancelButton: Button? = null
        var removeButton: Button? = null

        init {
            txtName = row?.findViewById(R.id.alarm_list_text)
            editButton = row?.findViewById(R.id.alarm_list_edit)
            cancelButton = row?.findViewById(R.id.alarm_list_cancel)
            removeButton = row?.findViewById(R.id.alarm_list_remove)
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.alarms_list, parent, false)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        val alarm = items[position]
        viewHolder.txtName?.text = alarm.toString()
        viewHolder.editButton?.setOnClickListener {
            AlarmSettingsFragment.newInstance(alarm).show(fragmentManager, "alarmSettings")
        }
        viewHolder.cancelButton?.setOnClickListener {
            if (items[position].isActive) {
                context?.cancelAlarm(items[position])
            } else {
                context?.scheduleAlarm(items[position], Intent(context, AlarmActivity::class.java).with {
                    putExtra(AlarmActivity.ALARM, gson.toJson(alarm))
                })
            }
            items[position] = items[position].copy(isActive = !items[position].isActive)
            notifyDataSetChanged()
        }
        viewHolder.removeButton?.setOnClickListener {
            context?.cancelAlarm(items[position])
            items.removeAt(position)
            sharedPrefs.edit {
                val alarms = fromJson<MutableList<MotivatingAlarm>>(sharedPrefs.getString(ALARM_STORE, "[]") ?: "[]")
                alarms.remove(alarm)
                it.putString(ALARM_STORE, gson.toJson(alarms))
            }
            notifyDataSetChanged()
        }

        return view
    }
}