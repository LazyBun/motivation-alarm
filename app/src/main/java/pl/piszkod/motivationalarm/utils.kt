package pl.piszkod.motivationalarm

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.fatboyindustrial.gsonjodatime.Converters
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import uy.klutter.core.common.whenNotNull

val gson = Converters.registerAll(GsonBuilder()).create()

inline fun <reified T> fromJson(s: String) = gson.fromJson<T>(s, object : TypeToken<T>() {}.type)

fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
    val editor = this.edit()
    operation(editor)
    editor.apply()
}

val PREFS_NAME = "MotiAlarmPrefs"
val ALARM_STORE = "SingleAlarmStore"

//TODO: Thread unsafe
val scheduledAlarms = mutableListOf<Pair<MotivatingAlarm, PendingIntent>>()

fun Context.scheduleAlarm(alarm: MotivatingAlarm, intent: Intent) {
    val pendingIntent = PendingIntent.getActivity(
            this,
            AlarmManager.RTC_WAKEUP,
            intent,
            PendingIntent.FLAG_CANCEL_CURRENT
    )
    (getSystemService(Context.ALARM_SERVICE) as AlarmManager).setExact(
            AlarmManager.RTC_WAKEUP,
            alarm.closestExecution().millis,
            pendingIntent
    )
    scheduledAlarms.add(alarm to pendingIntent)
}

fun Context.cancelAlarm(alarm: MotivatingAlarm) {
    scheduledAlarms.find { it.first == alarm }?.whenNotNull {
        (getSystemService(Context.ALARM_SERVICE) as AlarmManager).cancel(it.second)
        scheduledAlarms.remove(it)
    }
}