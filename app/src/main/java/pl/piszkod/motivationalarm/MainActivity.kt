package pl.piszkod.motivationalarm

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ListView
import org.joda.time.DateTime
import uy.klutter.core.common.whenNotNull
import uy.klutter.core.common.with


//TODO: Boot receiver (to reschedule alarms)
class MainActivity : Activity(), AlarmSettingsFragment.OnAlarmSetListener {

    val sharedPrefs: SharedPreferences by lazy {
        getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    var alarmList: ListView? = null

    var alarms: MutableList<MotivatingAlarm> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        alarms = fromJson(sharedPrefs.getString(ALARM_STORE, "[]") ?: "[]")

        alarms.filter { it.isActive }.forEach { alarm ->
            scheduleAlarm(alarm, Intent(this, AlarmActivity::class.java).with {
                putExtra(AlarmActivity.ALARM, gson.toJson(alarm))
            })
        }

        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.set_alarm_button).setOnClickListener {
            AlarmSettingsFragment.newInstance(null).show(fragmentManager, "alarmSettings")
        }

        alarmList = findViewById(R.id.alarm_list)
        alarmList?.adapter = MotivatingAlarmAdapter(this, alarms, fragmentManager)
    }

    override fun onAlarmSet(alarm: MotivatingAlarm, previousAlarm: MotivatingAlarm?) {
        Log.d("Alarm", "Alarm set: $alarm")
        previousAlarm.whenNotNull { previous ->
            cancelAlarm(previous)
            //TODO: Just update would be best. Now edited element will be last on list
            alarms.remove(previous)
        }
        alarms.add(alarm)
        sharedPrefs.edit {
            it.putString(ALARM_STORE, gson.toJson(alarms))
        }
        scheduleAlarm(alarm, Intent(this, AlarmActivity::class.java).with {
            putExtra(AlarmActivity.ALARM, gson.toJson(alarm))
        })
        //TODO: Make this not ugly
        (alarmList?.adapter as MotivatingAlarmAdapter).notifyDataSetChanged()
    }

}