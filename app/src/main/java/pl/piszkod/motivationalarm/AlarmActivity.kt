package pl.piszkod.motivationalarm

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.widget.Button
import org.joda.time.DateTime
import uy.klutter.core.common.with

class AlarmActivity : Activity() {

    val player: MediaPlayer by lazy {
        MediaPlayer.create(this, R.raw.test_quote)
    }

    val sharedPrefs: SharedPreferences by lazy {
        getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    companion object {
        val ALARM = "alarm"
        val TAG = "AlarmActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarm)

        Log.d("AlarmActivity", "Time: ${DateTime.now()}")
        val parcel = intent?.extras ?: throw IllegalStateException()

        val alarm = gson.fromJson(parcel.getString(ALARM), MotivatingAlarm::class.java) ?: throw IllegalStateException()

        cancelAlarm(alarm)
        if(alarm.isRecurring) {
            Log.i(TAG, "Ran recurring alarm")
            scheduleAlarm(alarm, Intent(this, AlarmActivity::class.java).with {
                putExtra(AlarmActivity.ALARM, gson.toJson(alarm))
            })
        } else {
            Log.i(TAG, "Ran single alarm")
            sharedPrefs.edit {
                val alarms = fromJson<MutableList<MotivatingAlarm>>(sharedPrefs.getString(ALARM_STORE, "[]") ?: "[]")
                alarms.remove(fromJson<MotivatingAlarm>(parcel.getString(ALARM) ?: throw IllegalStateException()))
                it.putString(ALARM_STORE, gson.toJson(alarms))
                //TODO: Update list
            }
        }

        findViewById<Button>(R.id.alarmActivity_closeButton).setOnClickListener {
            Log.i(TAG, "Closing alarm activity")
            finish()
        }

        player.start()
    }

}
