package pl.piszkod.motivationalarm

import android.app.DialogFragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import org.joda.time.DateTime
import org.joda.time.LocalTime
import uy.klutter.core.common.whenNotNull
import uy.klutter.core.common.with

class AlarmSettingsFragment : DialogFragment() {

    private var alarmParam: MotivatingAlarm? = null

    private var callback: OnAlarmSetListener? = null

    private var daysMap: Map<Day, CheckBox> = mapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            alarmParam = gson.fromJson(arguments.getString(ALARM_PARAM), MotivatingAlarm::class.java)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_alarm_settings, container, false)

        val tabHost: TabHost = view.findViewById(R.id.alarm_settings_tab_host)
        tabHost.setup()
        tabHost.addTab(tabHost.newTabSpec("Recurring").with {
            setContent(R.id.alarm_settings_tab1)
            setIndicator("Recurring")
        })
        tabHost.addTab(tabHost.newTabSpec("Single").with {
            setContent(R.id.alarm_settings_tab2)
            setIndicator("Single")
        })

        initializeRecurringAlarmSettings(view)

        initializeSingleAlarmSettings(view)

        return view
    }

    private fun initializeSingleAlarmSettings(view: View) {
        val timePicker = view.findViewById<TimePicker>(R.id.singleAlarmTimePicker)
        timePicker.setIs24HourView(true)
        val datePicker = view.findViewById<DatePicker>(R.id.singleAlarmDatePicker)
        alarmParam.whenNotNull{
            timePicker.minute = it.time.minuteOfHour
            timePicker.hour = it.time.hourOfDay

            datePicker.init(it.time.year, it.time.monthOfYear+1, it.time.dayOfMonth, {_,_,_,_ -> Unit})
        }

        view.findViewById<Button>(R.id.saveSingleButton).setOnClickListener {
            callback?.onAlarmSet(MotivatingAlarm(
                    DateTime(datePicker.year, datePicker.month+1, datePicker.dayOfMonth, timePicker.hour, timePicker.minute),
                    listOf(),
                    false
            ), alarmParam)
            dismiss()
        }

        view.findViewById<Button>(R.id.cancelSingleButton).setOnClickListener {
            dismiss()
        }

    }

    private fun initializeRecurringAlarmSettings(view: View) {
        val timePicker = view.findViewById<TimePicker>(R.id.recurringAlarmTimePicker)
        timePicker.setIs24HourView(true)

        daysMap = fillDays(view)

        alarmParam.whenNotNull{
            timePicker.minute = it.time.minuteOfHour
            timePicker.hour = it.time.hourOfDay

            daysMap.filter { checkbox -> it.days.contains(checkbox.key.id) }.forEach {
                it.value.isChecked = true
            }
        }

        view.findViewById<Button>(R.id.saveRecurringButton).setOnClickListener {
            callback?.onAlarmSet(MotivatingAlarm(
                    LocalTime(timePicker.hour, timePicker.minute).toDateTimeToday(),
                    daysMap.filter { it.value.isChecked }.map { it.key.id },
                    true
            ), alarmParam)
            dismiss()
        }

        view.findViewById<Button>(R.id.cancelRecurringButton).setOnClickListener {
            dismiss()
        }

        view.findViewById<Button>(R.id.selectAllButton).setOnClickListener {
            daysMap.forEach {
                it.value.isChecked = true
            }
        }

        view.findViewById<Button>(R.id.selectWeekendButton).setOnClickListener {
            daysMap[Day.MONDAY]?.isChecked = false
            daysMap[Day.TUESDAY]?.isChecked = false
            daysMap[Day.WEDNESDAY]?.isChecked = false
            daysMap[Day.THURSDAY]?.isChecked = false
            daysMap[Day.FRIDAY]?.isChecked = false
            daysMap[Day.SATURDAY]?.isChecked = true
            daysMap[Day.SUNDAY]?.isChecked = true
        }

        view.findViewById<Button>(R.id.selectWeekdaysButton).setOnClickListener {
            daysMap[Day.MONDAY]?.isChecked = true
            daysMap[Day.TUESDAY]?.isChecked = true
            daysMap[Day.WEDNESDAY]?.isChecked = true
            daysMap[Day.THURSDAY]?.isChecked = true
            daysMap[Day.FRIDAY]?.isChecked = true
            daysMap[Day.SATURDAY]?.isChecked = false
            daysMap[Day.SUNDAY]?.isChecked = false
        }

        view.findViewById<Button>(R.id.deselectAllButton).setOnClickListener {
            daysMap.forEach {
                it.value.isChecked = false
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnAlarmSetListener) {
            callback = context
        } else {
            throw RuntimeException("$context must implement OnAlarmSetListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }

    interface OnAlarmSetListener {
        fun onAlarmSet(alarm: MotivatingAlarm, previousAlarm: MotivatingAlarm?)
    }

    companion object {
        private val ALARM_PARAM = "alarm"

        fun newInstance(alarm: MotivatingAlarm?): AlarmSettingsFragment {
            val fragment = AlarmSettingsFragment()
            val args = Bundle()
            if (alarm != null) {
                args.putString(ALARM_PARAM, gson.toJson(alarm))
            }
            fragment.arguments = args
            return fragment
        }
    }


    private fun fillDays(view: View) = mapOf(
            Day.MONDAY to view.findViewById<CheckBox>(R.id.mondayCheckbox),
            Day.TUESDAY to view.findViewById(R.id.tuesdayCheckbox),
            Day.WEDNESDAY to view.findViewById(R.id.wednesdayCheckbox),
            Day.THURSDAY to view.findViewById(R.id.thursdayCheckbox),
            Day.FRIDAY to view.findViewById(R.id.fridayCheckbox),
            Day.SATURDAY to view.findViewById(R.id.saturdayCheckbox),
            Day.SUNDAY to view.findViewById(R.id.sundayCheckbox)
    )
}

//TODO: Merge Recurring alarm with single alarm
data class MotivatingAlarm(
        val time: DateTime,
        val days: List<Int> = listOf(),
        val isRecurring: Boolean = false,
        val isActive: Boolean = true
) {
    fun closestExecution(): DateTime = if (isRecurring) {
        val localTime = time.toLocalTime()
        val now = DateTime.now()
        val day = if (now.toLocalTime() > localTime) now.dayOfWeek().get() + 1 else now.dayOfWeek().get()
        val dayToSchedule = (days.find { it >= day } ?: {
            (7 - now.dayOfWeek().get()) + (days.firstOrNull() ?: throw IllegalStateException())
        }()) - now.dayOfWeek().get()

        now.withTime(localTime).plusDays(dayToSchedule)
    } else {
        time
    }
}

enum class Day(val id: Int) {
    MONDAY(1), TUESDAY(2), WEDNESDAY(3), THURSDAY(4), FRIDAY(5), SATURDAY(6), SUNDAY(7)
}












